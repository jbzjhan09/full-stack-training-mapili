import math


create_space = lambda sp_count: ''.join([' ' for x in xrange(sp_count)])
create_asterisk = lambda ast_count: ''.join(['* ' for x in xrange(ast_count)])
get_perfect_square = lambda l: [x for x in xrange((l / 2) + 2) if x * x == l]
get_alt_perfect_square = lambda l : (math.sqrt(l) % 1) == 0

def print_pyramid_stack(n):
	level1 = [create_space(n - x + n) + create_asterisk(x)  + '\n' for x in xrange(1, n + 1)]
	level2 = [create_space(n - x) + create_asterisk(x) + create_space((n - x) * 2) + create_asterisk(x)  + '\n' for x in xrange(1, n + 1)]
	return ''.join(level1) + ''.join(level2)


num_list = [81, 14, 12, 16, 1, 4, 20, 25, 8, 3, 1]
print list(filter(get_perfect_square, num_list))
print list(filter(get_alt_perfect_square, num_list))
print print_pyramid_stack(3)