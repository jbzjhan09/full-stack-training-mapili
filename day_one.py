def print_triangle(n):
	for x in range(n):
		level = ''
		for y in range(x + 1):
			level += '1'
		print level

def print_triangle_b(n, mul = 1):
	level1 = ''
	for x in range(n):
		level1 = get_ast(x, n, mul)
		print level1

def print_pyramid(n):
	print_triangle_b(n, 2)
	for x in range(n):
		level2 = get_ast(x, n)
		print level2 + level2

#function stack
def  get_ast(x, n, mul = 1):
	line = ''
	space = ''
	x = x + 1
	for y in range((n * mul) - x):
		space += ' '
 	for y in range(x):
 		line += '1 '
 	return space + line + space

def print_pyramid_stack(n):
	for x in range(n):
		level = ''
		for y in range(n - x + 1 + n):
			level += ' '
		for y in range(x + 1):
			level += ' *'
		print level
 	for x in range(n):
 		level = ''
 		for y in range(n - x + 1):
 			level += ' '
 		for y in range(x + 1):
 			level += ' *'
 		for y in range(n - x - 1):
 			level += ' '
 		for y in range(n - x):
 			level += ' '
 		for y in range(x + 1):
 			level += '* '
 		print level


# print_triangle(4)
# print_triangle_b(3)
# print_pyramid(4)
print_pyramid_stack(5)